//
//  SSHelloer.m
//  SampleSubproject
//
//  Created by James Stewart on 1/6/13.
//  Copyright (c) 2013 StewartStuff. All rights reserved.
//

#import "SSHelloer.h"

@implementation SSHelloer

- (NSString *)hello {
    return @"Hello form SSHelloer in SampleSubproject";
}
@end
